# Hey !

The purpose of this tool is to create a backup of all `bz2` files on 
minio instance.

Please make sure to create a `config.php` and configure it before
running this tool.

```text
$ cp config.sample.php config.php
$ $EDITOR config.php
$ php8.1 backup.php
```
