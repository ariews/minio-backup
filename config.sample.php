<?php

declare(strict_types=1);

/*
 * This file is part of Amorid Project
 *
 * (c) Arie W. Subagja <arie@malam.or.id>
 */

$localSource = '/path/to/source-dir';
$minioOptions = [
    'region' => 'us-east-1',
    'endpoint' => 'http://minio-instance.com:9000',
    'accessKeyId' => 'a-key',
    'accessKeySecret' => 'a-secret-key',
];
$minioBucket = 'bucket-name';
$minioBucketPrefix = '';
