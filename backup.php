<?php

declare(strict_types=1);

/*
 * This file is part of Amorid Project
 *
 * (c) Arie W. Subagja <arie@malam.or.id>
 */

use AsyncAws\S3\S3Client;
use League\Flysystem\AsyncAwsS3\AsyncAwsS3Adapter;
use League\Flysystem\FileAttributes;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\MountManager;
use League\Flysystem\StorageAttributes;

require __DIR__.'/vendor/autoload.php';

$configPath = __DIR__.'/config.php';
if (!file_exists($configPath)) {
    echo 'config.php file does not exists';
    exit(1);
}

/** @var string $localSource */
require $configPath;

$localAdapter = new LocalFilesystemAdapter($localSource);
$localFilesystem = new Filesystem($localAdapter);

/**
 * @var array  $minioOptions
 * @var string $minioBucket
 * @var string $minioBucketPrefix
 */
$s3AsyncClient = new S3Client($minioOptions);
$s3AsyncAdapter = new AsyncAwsS3Adapter($s3AsyncClient, $minioBucket, $minioBucketPrefix);
$s3AsyncFilesystem = new Filesystem($s3AsyncAdapter);

$manager = new MountManager([
    'local' => $localFilesystem,
    'minio' => $s3AsyncFilesystem,
]);

/* @var FileAttributes[] $files */
try {
    $files = $manager->listContents('local:///', true)
        ->filter(fn (StorageAttributes $attributes) => $attributes->isFile() && 'bz2' === pathinfo($attributes->path(), \PATHINFO_EXTENSION))
        ->sortByPath()
        ->toArray();
} catch (FilesystemException $e) {
    echo sprintf("failed to get list of files: %s\n", $e->getMessage());
    exit(1);
}

$total = count($files);
$i = 0;
foreach ($files as $file) {
    ++$i;
    $storagePath = preg_replace('{^local://}', 'minio://', $file->path());

    try {
        if ($manager->fileExists($storagePath)) {
            if ($manager->fileSize($storagePath) === $file->fileSize()) {
                echo sprintf("[%d/%d] skipped, file exists %s\n", $i, $total, $storagePath);
                continue;
            }
            $manager->delete($storagePath);
        }

        $stream = $manager->readStream($file->path());
        if (!$stream) {
            echo sprintf("[%d/%d] failed to read stream %s\n", $i, $total, $file->path());
            continue;
        }

        $manager->writeStream($storagePath, $stream);
        echo sprintf("[%d/%d] copied %s to %s\n", $i, $total, $file->path(), $storagePath);
    } catch (FilesystemException $e) {
        echo sprintf("[%d/%d] failed to copy %s: %s\n", $i, $total, $file->path(), $e->getMessage());
    }
}
